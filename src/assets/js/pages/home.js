import Vue from 'vue';
import axios from 'axios';

export let $vueInstance = () => {

  new Vue({

    el: '#app',

    data: {
      message: 'Listagem de dados vindos do NodeJS!',
      posts: [],
      users: []
    },

    mounted() {
      this.getPosts();
      this.getUsers();
      this.getComments();
      this.setPost();
      // this.updatePost();
      this.deletePost();
    },

    methods: {

      getPosts: function () {

        let data = document.getElementById('data').value;

        this.posts = JSON.parse(data)[0];

      },

      getUsers: function () {

        let data = document.getElementById('data').value;

        this.users = JSON.parse(data)[1];

      },

      getComments: function () {

        axios
          .get('/examples/comments')
          .then(response => {
            console.log('get: ', response);
          })
          .catch(err => console.log(err));

      },

      setPost: function() {

        axios
          .post('/examples/send-post', {
            title: 'foo',
            body: 'bar',
            userId: 1
          })
          .then(response => {
            console.log('post: ', response);
          })
          .catch(err => console.log(err));

      },

      updatePost: function() {

        axios
          .put('/examples/update-post/1', {
            id: 1,
            title: 'foo',
            body: 'bar',
            userId: 1
          })
          .then(response => {
            console.log('put: ', response);
          })
          .catch(err => console.log(err));

      },

      deletePost: function() {

        axios
          .delete('/examples/delete-post/1')
          .then(response => {
            console.log('delete: ', response);
          })
          .catch(err => console.log(err));

      }

    }

  });

}
