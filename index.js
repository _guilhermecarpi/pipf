// Express
const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const expressSession = require('express-session');

// Utils
const memoryStore = require('memorystore')(expressSession);
const cookieParser = require('cookie-parser');
const dotenv = require('dotenv').config({path: './config/.env'});
const helmet = require('helmet');
const path = require('path');

// Initialize Express
const app = express();

// Middlewares
app.use(helmet.noCache());
app.use(helmet.frameguard());
app.use(expressLayouts);
app.use(cookieParser());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use('*/img',  express.static('dist/images'));
app.use('*/css', express.static('dist/css'));
app.use('*/js', express.static('dist/js'));
app.use('/', express.static(__dirname));

app.use(expressSession({
  store: new memoryStore({ checkPeriod: 1800000 }),
  secret: 'very very secret',
  resave: true,
  saveUninitialized: false
}));

// View config
app.set('view engine', 'ejs');
app.set('view cache', false);
app.set('views', [ path.join(__dirname, 'src/views')]);

// Server listen to .env port
app.listen(process.env.PORT);

// Application controller
app.use(require('./app/routes'));

