// Libraries
const express = require('express');

// Services
const sender = require('../services/sender');

// Utils
const router = express.Router();
const API_URL = process.env.API;

// Routes
router.get('/comments', (req, res) => {
  sender.request(req, res, {
    auth: true,
    method: 'get',
    uri: `${API_URL}/comments`
  });
});

router.post('/send-post', (req, res) => {
  sender.request(req, res, {
    method: 'post',
    uri: `${API_URL}/posts`
  });
});

router.put('/update-post/:id', (req, res) => {
  sender.request(req, res, {
    method: 'put',
    uri: `${API_URL}/posts/${req.params.id}`
  });
});

router.delete('/delete-post/:id', (req, res) => {
  sender.request(req, res, {
    method: 'delete',
    uri: `${API_URL}/posts/${req.params.id}`
  });
});

module.exports = router;
