// Libraries
const express = require('express');

// Services
const render = require('./services/render');

// Utils
const API_URL = process.env.API;
const router = express.Router();

// Routes
router.use('/examples', require('./controllers/example'))

router.get('/', (req, res) => {
  render(req, res, {
    page: 'index',
    title: 'Página Inicial',
    uri: [
      `${API_URL}/posts`,
      `${API_URL}/users`
    ]
  });
});

router.get('/about', (req, res) => {
  render(req, res, {
    auth: true,
    page: 'about',
    title: 'Sobre nós',
  });
});

module.exports = router;
