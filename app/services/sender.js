// Utils
import { getAuth, handleRequest } from '../utils/request';

module.exports = {

  request: (req, res, params) => get(req, res, params)

}

async function get(req, res, params) {

  if (params.auth) params.token = getAuth(req, res);

  if (params.method === 'post' || params.method === 'put') params.data = req.body;

  let $response = await handleRequest(params);

  res.json($response);

}
