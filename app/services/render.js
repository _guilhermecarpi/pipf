// Libraries
const axios = require('axios');

// Utils
import { getAuth } from '../utils/request';

let render = (req, res, params) => {

  let page = params.page,
    redirectTo = params.redirectTo ? params.redirectTo : 'login',
    opts = {
      headers: {
        "Content-type": 'application/json; charset=UTF-8',
        "User-Agent": 'PIPF'
      }
    },
    ext = '.ejs',
    renderOpts = {
      title: params.title ? params.title : process.env.PROJECT_NAME,
      layout: params.layout ? params.layout : 'layout',
      template: page
    };

  if (params.auth) opts.headers.authorization = getAuth(req, res, redirectTo);

  if (params.uri) {

    let calls = [];
    let data = [];

    params.uri.map(item => calls.push(axios.get(item, opts)));

    axios
      .all(calls)
      .then(response => {

        response.map((item) => {
          data.push(item.data)
        });

        renderOpts.error = false;
        renderOpts.data = JSON.stringify(data);

        res.render(page + ext, renderOpts);

      }).catch(err => {

        renderOpts.error = true;
        renderOpts.data = err;

        res.render(page + ext, renderOpts);

      });

  } else {

    res.render(page + ext, renderOpts);

  }

}

module.exports = render;
